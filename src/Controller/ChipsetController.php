<?php

namespace App\Controller;

use App\Entity\Chipset;
use App\Entity\Manufacturer;
use App\Form\Chipset\Search;
use App\Repository\ChipsetRepository;
use App\Repository\ManufacturerRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChipsetController extends AbstractController
{
    #[Route(path: '/chipsets/{id}', name: 'chipset_show', requirements: ['id' => '\d+'])]
    public function show(int $id, ChipsetRepository $chipsetRepository)
    {
        $chipset = $chipsetRepository->find($id);
        if (!$chipset) {
            throw $this->createNotFoundException(
                'No chipset found for id ' . $id
            );
        } else {
            return $this->render('chipset/show.html.twig', [
                'chipset' => $chipset,
                'controller_name' => 'ChipsetController',
            ]);
        }
    }


    #[Route(path: '/chipsets/', name: 'chipsetsearch', methods: ['GET'])]
    public function searchResult(Request $request, PaginatorInterface $paginator, ChipsetRepository $chipsetRepository)
    {
        $criterias = array();
        $name = htmlentities($request->query->get('name') ?? '');
        if ($name) {
            $criterias['name'] = "$name";
        }
        $chipsetId = htmlentities($request->query->get('chipsetId') ?? '');
        if ($chipsetId && intval($chipsetId)) {
            $criterias['chipset'] = "$chipsetId";
        } elseif ($chipsetId === "NULL") {
            $criterias['chipset'] = null;
        }
        $chipsetManufacturerId = htmlentities($request->query->get('chipsetManufacturerId') ?? '');
        if (
            $chipsetManufacturerId
            &&
            intval($chipsetManufacturerId)
            &&
            !array_key_exists('chipset', $criterias)
        ) {
            $criterias['manufacturer'] = "$chipsetManufacturerId";
        } elseif ($chipsetManufacturerId === "NULL" && !array_key_exists('chipset', $criterias)) {
            $criterias['manufacturer'] = null;
        }
        $showImages = boolval(htmlentities($request->query->get('showImages')));
        if ($criterias == array()) {
            return $this->redirectToRoute('chipset_search');
        }
        try {
            $data = $chipsetRepository->findByChipset($criterias);
        } catch (Exception $e) {
            return $this->redirectToRoute('chipset_search');
        }
        $chipsets = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            $this->getParameter('app.pagination.max')
        );
        return $this->render('chipset/result.html.twig', [
            'controller_name' => 'ChipsetController',
            'chipsets' => $chipsets,
            'chipset_count' => count($data),
            'show_images' => $showImages,
        ]);
    }


    #[Route(path: '/chipsets/search/', name: 'chipset_search')]
    public function search(Request $request, TranslatorInterface $translator, ManufacturerRepository $manufacturerRepository)
    {
        $notIdentifiedMessage = $translator->trans("Not identified");
        $chipsetManufacturers = $manufacturerRepository->findAllChipsetManufacturer();
        $unidentifiedMan = new Manufacturer();
        $unidentifiedMan->setName($notIdentifiedMessage);
        array_unshift($chipsetManufacturers, $unidentifiedMan);
        $form = $this->createForm(Search::class, array(), [
            'chipsetManufacturers' => $chipsetManufacturers,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirect($this->generateUrl('chipsetsearch', $this->searchFormToParam($request, $form)));
        }
        return $this->render('chipset/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function searchFormToParam(Request $request, $form): array
    {
        $parameters = array();

        if ($form['searchWithImages']->isClicked()) {
            $parameters['showImages'] = true;
        }

        if ($form['chipsetManufacturer']->getData()) {
            if ($form['chipsetManufacturer']->getData()->getId() == 0) {
                $parameters['chipsetManufacturerId']  = "NULL";
            } else {
                $parameters['chipsetManufacturerId'] = $form['chipsetManufacturer']->getData()->getId();
            }
        }
        $parameters['name'] = $form['name']->getData();

        return $parameters;
    }


    #[Route(path: '/chipsets/index/{letter}', name: 'chipsetindex', requirements: ['letter' => '\w'])]
    public function index(PaginatorInterface $paginator, string $letter, ChipsetRepository $chipsetRepository, Request $request)
    {
        $data = $chipsetRepository->findAllAlphabetic($letter);
        $chipsets = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            $this->getParameter('app.pagination.max')
        );
        return $this->render('chipset/index.html.twig', [
            'chipsets' => $chipsets,
            'chipset_count' => count($data),
            'letter' => $letter,
        ]);
    }
}
